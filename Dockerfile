FROM registry.access.redhat.com/openjdk/openjdk-11-rhel7:1.10
COPY nl-gateway-impl/target/nl-gateway-impl.jar nl-gateway-impl.jar
ENV _JAVA_OPTIONS="-Xms128m -Xmx1024m -Dfile.encoding=UTF8"
ENTRYPOINT ["java","-jar","nl-gateway-impl.jar","--spring.profiles.active=local"]
EXPOSE 8080