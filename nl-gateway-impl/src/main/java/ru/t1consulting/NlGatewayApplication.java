package ru.t1consulting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NlGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NlGatewayApplication.class,args);
    }
}
